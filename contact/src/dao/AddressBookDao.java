package dao;

import model.Contact;

//Dao模式
public interface AddressBookDao {
	//添加联系人
	public boolean addContact(Contact contact);
	//删除联系人
	public boolean deleteContact(String name);
	//查找联系人
	public Contact seekContace(String name);
	//输出联系人
	public void printContact();
}
