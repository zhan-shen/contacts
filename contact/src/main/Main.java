package main;

import java.util.Scanner;

import model.AddressBookDaoListImpl;
import model.Contact;
import model.User;
import model.UserDaoImpl;

/**
 * 
 * @author AIERXUAN
 *
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		Scanner sc = new Scanner(System.in);

		// 登录注册
		boolean flag = true;
		while (flag) {
			System.out.println("注册请按1，登录请按2");
			User user = new User();
			if (Integer.parseInt(sc.next()) == 1) {
				while (true) {
					System.out.print("请输入注册账号:");
					user.setNumber(sc.next());
					System.out.print("请输入注册密码:");
					user.setPassword(sc.next());
					if (userDaoImpl.addUser(user)) {
						System.out.println("注册成功!");
						break;
					} else {
						System.out.println("账号已存在");
					}
				}
			} else {
				while (true) {
					System.out.print("请输入账号:");
					user.setNumber(sc.next());
					System.out.print("请输入密码:");
					user.setPassword(sc.next());
					if (userDaoImpl.matchAccount(user)) {
						System.out.println("登录成功！");
						flag = false;
						break;
					} else {
						System.out.println("账号不存在或密码错误!");
					}
				}
			}

		}

		// 进入个人通讯录
		AddressBookDaoListImpl addressBookDaoListImpl=new AddressBookDaoListImpl();
		System.out.println("************************");
		System.out.println("**    1.添加联系人    **");
		System.out.println("**    2.删除联系人    **");
		System.out.println("**    3.查找联系人    **");
		System.out.println("**    4.退出该系统    **");
		System.out.println("************************");
		while (true) {
			System.out.println("通讯录:");
			addressBookDaoListImpl.printContact();
			System.out.print("请输入对应功能的序号:");
			int temp = Integer.parseInt(sc.next());
			switch (temp) {
			case 1:
				Contact contact=new Contact(null, null, null);
				System.out.print("请输入联系人的姓名：");
				contact.setName(sc.next());
				System.out.print("请输入联系人的号码：");
				contact.setNumber(sc.next());
				System.out.print("请输入联系人的备注：");
				contact.setRemarks(sc.next());
				addressBookDaoListImpl.addContact(contact);
				break;
			case 2:
				System.out.print("请输入需要删除的联系人姓名:");
				addressBookDaoListImpl.deleteContact(sc.next());
				break;
			case 3:
				System.out.print("请输入需要查找的联系人姓名:");
				System.out.println(addressBookDaoListImpl.seekContace(sc.next()).toString());
				break;
			case 4:
				return;
			default:
					System.out.println("输入错误！");
			}
		}
	}

}
