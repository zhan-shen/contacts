package model;

//联系人类
public class Contact {
	private String name;// 姓名
	private String number;// 号码
	private String remarks;// 备注

	public Contact(String name, String number, String remarks) {
		this.name = name;
		this.number = number;
		this.remarks = remarks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return name + "    " + number + "    " + remarks;
	}

}
