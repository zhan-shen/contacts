package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dao.UserDao;

//用户类
public class UserDaoImpl implements UserDao{

	private List<User> userList=new ArrayList<>();
	
	//无参构造
	public UserDaoImpl() {
		this.openUserFile();
	}
	
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	@Override
	public boolean addUser(User user) {
		for (User e : userList) {
			if(e.getNumber().equals(user.getNumber())) {//已有账号信息
				return false;
			}
		}
		
		userList.add(user);
		
		//保存到文件中
		saveFile(userList);
		return true;
	}

	@Override
	public boolean matchAccount(User user) {
		for (User e : userList) {
			if(e.getNumber().equals(user.getNumber())) {//账号匹配
				if(e.getPassword().equals(user.getPassword())){//密码匹配
					return true;//匹配成功
				}
			}
		}
		return false;
	}

	
	// 将文件信息载入列表
	public void openUserFile() {
		BufferedReader input = null;
		try {
			input = new BufferedReader(new FileReader("User.txt"));
			//读取每一行的账号密码写入列表中
			String l;
			String[] str=new String[2];
			try {
				while ((l = input.readLine()) != null) {
					str=l.split(" ");
					userList.add(new User(str[0],str[1]));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	//将注册账号保存到文件中
	public void saveFile(List<User> userList) {
		BufferedWriter output = null;
		try {
			output=new BufferedWriter(new FileWriter("User.txt"));
			for (User e : userList) {
				output.write(e.getNumber()+" "+e.getPassword());
				output.newLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
