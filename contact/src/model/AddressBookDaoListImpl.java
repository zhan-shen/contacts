package model;

import java.util.ArrayList;
import java.util.List;

import dao.AddressBookDao;

/*
 ͨѶ¼��
*/
public class AddressBookDaoListImpl implements AddressBookDao{
	private List<Contact> contacts=new ArrayList<>();

	
	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	@Override
	public boolean addContact(Contact contact) {
		contacts.add(contact);
		return true;
	}

	@Override
	public boolean deleteContact(String name) {
		for(int i=0;i<contacts.size();i++) {
			if(contacts.get(i).getName().equals(name)) {
				contacts.remove(i--);
			}
		}
		return true;
	}

	@Override
	public Contact seekContace(String name) {
		for (Contact e : contacts) {
			if(e.getName().equals(name)) {
				return e;
			}
		}
		return null;
	}

	@Override
	public void printContact() {
		// TODO Auto-generated method stub
		for (Contact e: contacts) {
			System.out.println(e.toString());
		}
	}
	
	
}